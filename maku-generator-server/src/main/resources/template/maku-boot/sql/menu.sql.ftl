<#assign dbTime = "now()">
<#if dbType=="SQLServer">
    <#assign dbTime = "getDate()">
</#if>

-- 初始化菜单
INSERT INTO PUB_SYSFUNCTION (APPNAME, FUNCTION_NO, PFUNCTION_ID, FUNCTION_EXPLAIN, FUNCTION_CODE, FUNCTION_MODE, EXPLAININDEX, FUNCTION_GROUP, PFUNCTION_ID_EN, CREATETIME, UPDATETIME, ISVALIDATE) VALUES ('MES', '${ClassName}View', null, '${tableComment}', null, null, null, null, null, sysdate, null, null);

${r'<!--struts.xml主配置文件添加 -->'}
<include file="struts-${className}.xml" />
${r'<!-- app-context.xml主配置文件添加-->'}
<import resource="app-${className}.xml" />
${r'<!-- app-resources.xml主配置文件添加-->'}
<value>classpath*:${packagePath}/${moduleName}/model/${ClassName}.hbm.xml</value>
${r'<!-- menu.xml 主配置文件 需要添加到适当位置-->'}
<Item id="${ClassName}View" iconCls="menu-tick" text="${tableComment!} "/>
${r'<!-- App.import.js 主配置文件,放到最下方即可-->'}
${ClassName}View:[__ctxPath + "/js/${moduleName}/${functionName}/${ClassName}View.js" ]
