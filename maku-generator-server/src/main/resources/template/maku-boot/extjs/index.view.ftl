Ext.ns('${ClassName}View');
${ClassName}View = function() {
	return this.init();
};
<!-- 页面高度 -->
${ClassName}View.prototype.inheight = MesScreen.getHeight();
<!-- 页面宽度 -->
${ClassName}View.prototype.inwidth = MesScreen.getWidth();
<!-- 加载动画 -->
${ClassName}View.prototype.mask = null;
${ClassName}View.prototype.showmask = function(msg, grid) {
	if (${ClassName}View.prototype.mask == null) {
		${ClassName}View.prototype.mask = new Ext.LoadMask(Ext.get(grid), {
			msg : msg
		});
	}
	${ClassName}View.prototype.mask.msg = msg;
	${ClassName}View.prototype.mask.show();
};
<!-- 页面刷新 -->
${ClassName}View.prototype.refresh = function() {
	var store = Ext.getStore('${className}Store');
	Ext.apply(store.proxy.extraParams, {
		<#list queryList as field>
		<#if field.queryFormType == 'date'|| field.queryFormType == 'datetime'>
			<#if field_index != 0>,</#if>${field.attrName}_start:Ext.getCmp('queryForm_${field.attrName}_start').getValue()
			,${field.attrName}_end:Ext.getCmp('queryForm_${field.attrName}_end').getValue()
		<#else>
			<#if field_index != 0>,</#if>${field.attrName}:Ext.getCmp('queryForm_${field.attrName}').getValue()
		</#if>
		</#list>
	});
	${ClassName}View.prototype.showmask("加载中...", "${className}Grid");
	store.loadPage(1);
	store.on("load", function(a, b, c, d) {
		if (c) {
			${ClassName}View.prototype.mask.hide();
		}
	});
};

<!-- 自动生成的model信息 -->
let ${className}Model = Ext.define('${className}Model', {
extend : 'Ext.data.Model',
fields : [<#list gridList as field><#if field_index != 0>,</#if>'${field.fieldName}'</#list>]
});
<!-- 自动生成的新增加更新的Form模型model信息 -->
let ${className}FormModel = Ext.define('${className}FormModel', {
	extend : 'Ext.data.Model',
	fields : [
	<#list fieldList as field>
	<#if field_index != 0>,</#if>
		{
			<!-- ${field.fieldComment!} -->
			<#if field.primaryPk >
			name: 'id'
			<#else >
			name: '${field.attrName}'
			</#if>
			,mapping: '${field.fieldName}'
		}
	</#list>
	]
});

<!-- 自动生成的store信息 -->
let ${className}Store = Ext.create('Ext.data.Store', {
	pageSize : 300,
	proxy : {
		type : 'ajax',
		actionMethods : {
			read : 'POST'
		},
		url : __ctxPath + "/${moduleName}/page${ClassName}Action.do",
		reader : {
			type : 'json',
			root : 'result',
			totalProperty : 'totalCounts'
		},
		extraParams : {

		}
	},
	storeId : '${className}Store',
	autoLoad : true,
	method : 'post',
	model : ${className}Model
	});
let pageBar = {
	xtype : 'pagingtoolbar',
	store : ${className}Store,
	displayInfo : true,
	dock : 'bottom'
}
let toolBar = {
	xtype: "toolbar",
	dock: "top",
	items: [
		{
			xtype: "button",
			iconCls: "btn-refresh",
			text: "刷新",
			id: '${ClassName}View_refresh',
			handler: function () {
				${ClassName}View.prototype.refresh();
			}
		},{
			xtype: 'button',
			text: "新增",
			icon: __ctxPath + '/images/btn/add_16.gif',
			id: '${ClassName}View_add',
			handler: function() {
				${ClassName}View.prototype.setForm(true);
			}
		}, {
			xtype: 'button',
			text: "修改",
			icon: __ctxPath + '/images/system/edit.gif',
			id: '${ClassName}View_EDIT',
			handler: function () {
				var gridSelect = Ext.getCmp("${className}Grid").getSelectionModel().getSelection();
				if (gridSelect.length !== 1) {
					Ext.ux.Toast.msg("提示", "请选择一个需要修改的数据!");
					return;
				}
				var id = gridSelect[0].data.ID;
				${ClassName}View.prototype.loadForm(id)
			}
		}, {
			xtype: 'button',
			text: "删除",
			icon: __ctxPath + '/images/btn/delete.gif',
			id: '${ClassName}View_delete',
			handler: function () {
				${className}Delete();
			}
		}
	]
};

let ${className}QueryForm = new Ext.form.Panel({
	region: 'north',
	id:'${ClassName}View_search',
	layout : {
		type : 'hbox'
	},
	height : 40,
	fontSize : '15px' ,
	defaults : {
		xtype : "textfield",
		labelAlign : "right",
		width: 220,
		labelWidth:70,
		anchor : '100%',
		margin : '4 4 4 4'
	},
	bodyStyle : {
		background : '#F0F8FF',
		padding : '5px'
	},
	items : [
<#list queryList as field>
	<#if field_index != 0>,</#if>
	<#if field.queryFormType == 'text' || field.queryFormType == 'textarea' || field.queryFormType == 'editor'>
		{
			xtype : 'textfield',
			fieldLabel : '${field.fieldComment!}',
			id : 'queryForm_${field.attrName}',
			labelWidth : 90,
			width : 200
		}
		<#elseif field.queryFormType == 'date'>
		{
			xtype : "datefield",
			format : "Y-m-d",
			fieldLabel : "${field.fieldComment!}从",
			anchor : '100%',
			editable : false,
			labelAlign : "right",
			id : 'queryForm_${field.attrName}_start',
			tooltip : '查询开始时间',
			listeners : {
				'blur' : function() {
					<!-- 设置结束时间不能小于开始时间 -->
					var beginTime = Ext.getCmp("queryForm_${field.attrName}_start").getValue();
					Ext.getCmp("queryForm_${field.attrName}_end").setMinValue(beginTime);
				}
			}
		}, {
			xtype : "datefield",
			format : "Y-m-d",
			anchor : '100%',
			editable : false,
			fieldLabel : "到",
			labelAlign : "right",
			id : 'queryForm_${field.attrName}_end',
			tooltip : '查询结束时间'
		}
		<#elseif field.queryFormType == 'datetime'>
		{
			xtype : "datetimefield",
			format : "Y-m-d",
			fieldLabel : "${field.fieldComment!}从",
			anchor : '100%',
			editable : false,
			labelAlign : "right",
			id : 'queryForm_${field.attrName}_start',
			tooltip : '查询开始时间',
			listeners : {
			'blur' : function() {
				<!-- 设置结束时间不能小于开始时间 -->
				var beginTime = Ext.getCmp("queryForm_${field.attrName}_start").getValue();
					Ext.getCmp("queryForm_${field.attrName}_end").setMinValue(beginTime);
				}
			}
		}, {
			xtype : "datefield",
			format : "Y-m-d",
			anchor : '100%',
			editable : false,
			fieldLabel : "到",
			labelAlign : "right",
			id : 'queryForm_${field.attrName}_end',
			tooltip : '查询结束时间'
		}
		<#else>
			{
				xtype : 'textfield',
				fieldLabel : '${field.fieldComment!}',
				id : 'queryForm_${field.attrName}'
			}
		</#if>
</#list>
		, {
			xtype: "button",
			text: "查询",
			width: 55,
			handler: function () {
				${ClassName}View.prototype.refresh();
			}
		}, {
			xtype: "button",
			text: "清空",
			width: 55,
			handler: function () {
				Ext.getCmp('${ClassName}View_search').getForm().reset();
				${ClassName}View.prototype.refresh();
			}
		}

<#--{
	xtype : 'textfield',
	fieldLabel : '生产订单号',
	id : '${ClassName}ViewSearchTaskno',
	listeners : {
	specialkey : function(field, e) {
	if (e.getKey() == e.ENTER) { ${ClassName}View.prototype.refresh(); }
	}
	}
	},{
	xtype : 'textfield',
	fieldLabel : '客户(名称)',
	id : '${ClassName}ViewSearchplanname',
	listeners : {
	specialkey : function(field, e) {
	if (e.getKey() == e.ENTER) { ${ClassName}View.prototype.refresh(); }
	}
	}
	},{
	xtype : 'textfield',
	fieldLabel : '销售订单号',
	id : '${ClassName}Viewplansysno',
	listeners : {
	specialkey : function(field, e) {
	if (e.getKey() == e.ENTER) { ${ClassName}View.prototype.refresh(); }
	}
	}
	}, {
	xtype : 'textfield',
	fieldLabel : '订单投产产品',
	id : '${ClassName}ViewSearchProdcode',
	labelWidth : 90,
	width : 300,
	listeners : {
	specialkey : function(field, e) {
	if (e.getKey() == e.ENTER) { ${ClassName}View.prototype.refresh(); }
	}
	}
	},{
	xtype : 'combo',
	fieldLabel : '订单状态',
	emptyText : "选择订单状态",
	editable : false,
	id : '${ClassName}ViewSearchPlanState',
	labelWidth : 60,
	width : 220,
	anchor : '100%',
	store : dictSelectStore_015,
	queryMode : 'local',
	valueField : 'DIC_CODE',
	displayField : 'DIC_NAME',
	value : '015000',
	listeners : {
	'render' : function(input) {
	Ext.getCmp("${ClassName}View_edit_beginTime").reset();
	Ext.getCmp("${ClassName}View_edit_endTime").reset();
	Ext.getCmp("${ClassName}View_btn_setQueryCondition").setValue('');
	${ClassName}View.prototype.refresh();
	}
	}
	}, {
	xtype : 'combo',
	id : '${ClassName}View_btn_setQueryCondition',
	fieldLabel : "日期类型",
	anchor : '100%',
	mode : "local",
	editable : false,
	value : "",
	triggerAction : "all",
	store : new Ext.data.ArrayStore({
	fields : ['value', 'display'],
	data : [['', '--选择日期类型--'], ['1', '订单录入时间  '], ['2', '订单启动时间 '],
	['3', '合同交付时间'], ['4', '实际结束时间 ']]
	}),
	valueField : 'value',
	displayField : 'display'
	}, {
	xtype : "datefield",
	format : "Y-m-d",
	fieldLabel : "从",
	anchor : '100%',
	editable : false,
	labelAlign : "right",
	id : '${ClassName}View_edit_beginTime',
	tooltip : '查询开始时间',
	listeners : {
	'blur' : function() {
	var beginTime = Ext.getCmp("${ClassName}View_edit_beginTime").getValue();
	Ext.getCmp("${ClassName}View_edit_endTime").setMinValue(beginTime);
	}
	}
	}, {
	xtype : "datefield",
	format : "Y-m-d",
	anchor : '100%',
	editable : false,
	fieldLabel : "到",
	labelAlign : "right",
	id : '${ClassName}View_edit_endTime',
	tooltip : '查询结束时间'
	},{
	xtype:'container',
	anchor : '100%',
	items:[{
	xtype : "button",
	text : "查询",
	width : 55,
	margin : '0 5 0 5',
	handler :function() {
	${ClassName}View.prototype.refresh();
	}
	},{
	xtype : "button",
	text : "清空",
	width : 55,
	margin : '0 5 0 5',
	handler :function() {
	Ext.getCmp("${ClassName}ViewSearchTaskno").reset();
	Ext.getCmp("${ClassName}ViewSearchplanname").reset();
	Ext.getCmp("${ClassName}ViewSearchProdcode").reset();
	Ext.getCmp("${ClassName}View_edit_beginTime").reset();
	Ext.getCmp("${ClassName}View_edit_endTime").reset();
	Ext.getCmp("${ClassName}View_btn_setQueryCondition").reset();
	Ext.getCmp("${ClassName}ViewSearchPlanState").reset();
	Ext.getCmp("${ClassName}Viewplansysno").reset();
	${ClassName}View.prototype.refresh();
	}
}-->]
});



<!-- 渲染的Panel信息 -->
let ${className}Grid = new Ext.grid.Panel({
	lines : true,
	height : '100%',
	flex : 1,
	selModel : Ext.create('Ext.selection.CheckboxModel', {
		mode : 'MULTI'
	}),
	region: 'center',
	split : true,
	multiSelect : true,
	id : '${className}Grid',
	store : ${className}Store,
	dockedItems : [,toolBar, pageBar],
	columns : [
		{
			xtype : 'rownumberer',
			text : '序号',
			width : 30
		}
	<#list gridList as field>
		,{
			header : '${field.fieldComment!}',
			flex : 1,
			dataIndex : '${field.fieldName}'
		}
	</#list>
 ]
});
/* 主界面初始化 */
${ClassName}View.prototype.init = function (het, wdt) {
	return Ext.create('Ext.panel.Panel', {
		id: '${ClassName}View',
		title: '${tableComment}',
		layout: 'border',
		closable: true,
		height: null == het ? this.inheight : het,
		width: null == wdt ? this.inwidth : wdt,
		items: [${className}QueryForm,${className}Grid]
	});
};

let ${className}Delete = function () {
	var grid = Ext.getCmp("${className}Grid");
	var item = grid.getSelectionModel().getSelection();
	var ppJsonArrays = [];
	if (item.length === 0) {
		Ext.ux.Toast.msg("提示", '至少选择一条记录！');
		return;
	}
	Ext.each(item, function (record) {
		ppJsonArrays.push(record.data.${primaryList[0].fieldName});
	});
	if (ppJsonArrays.length > 0) {
		Ext.Msg.confirm('提示', '请确认操作，是否继续？', function (a) {
			if (a === 'yes') {
				Ext.Ajax.request({
					method: "post",
					url: __ctxPath + "/${moduleName}/delete${ClassName}Action.do",
					params: {
						list: Ext.JSON.encode(ppJsonArrays)
					},
					success: function (response, config) {
						var json = Ext.JSON.decode(response.responseText);
						var result = json.result[0];
						Ext.ux.Toast.msg("提示", result);
						${ClassName}View.prototype.refresh();
						return;
					},
					failure: function (response, config) {
						var json = Ext.JSON.decode(response.responseText);
						Ext.ux.Toast.msg("提示", '删除失败');
						return;
					}
				});
			}
		});
	} else {
		Ext.ux.Toast.msg("提示", '没有数据被删除');
		return;
	}
};

${ClassName}View.prototype.loadForm = function (id) {
	${ClassName}View.prototype.setForm(false);
	var loadform = Ext.getCmp("${className}WindowForm");
	loadform.load({
		deferredRender: false,
		method: "post",
		/* 根据主键获取对象 */
		url: __ctxPath + "/${moduleName}/get${ClassName}Action.do",
		params: {
			"id": id
		},
		waitMsg: "正在载入数据...",
		success: function (d, e) {
		},
		failure: function (b, c) {
			Ext.MessageBox.alert("编辑", "载入失败");
		return;
		}
	});
};

${ClassName}View.prototype.setForm = function (isSave) {
	var reader = new Ext.data.reader.Json({
		root: "result",
		model: "${className}FormModel"
	});

	var container = new Ext.form.FieldContainer({
		layout: { type: "vbox" },
		width: 920,
		height:500,
		items: [
			<#if formLayout == 1 >
			{ xtype: "fieldcontainer", layout: { type: "hbox" },width:430,items: [
			<#list formList as field>
				<#if field_index != 0>,</#if>
				<#if field.formType == 'text'>
					{
					xtype: 'textfield',
					fieldLabel: '${field.fieldComment!}',
					id: 'EDIT_${field.attrName}',
					name: '${field.attrName}',
					<#if field.formRequired && !field.primaryPk>
						allowBlank: false,
					</#if>
					<#if field.primaryPk>
						hidden: true
					</#if>
					}
				<#elseif field.formType == 'textarea'>
					{
					xtype: 'textfield',
					fieldLabel: '${field.fieldComment!}',
					id: 'EDIT_${field.attrName}',
					name: '${field.attrName}',
					<#if field.formRequired && !field.primaryPk>
						allowBlank: false,
					</#if>
					<#if field.primaryPk>
						hidden: true
					</#if>
					}
				<#elseif field.formType == 'editor'>
					{
					xtype: 'textfield',
					fieldLabel: '${field.fieldComment!}',
					id: 'EDIT_${field.attrName}',
					name: '${field.attrName}',
					<#if field.formRequired && !field.primaryPk>
						allowBlank: false,
					</#if>
					<#if field.primaryPk>
						hidden: true
					</#if>
					}
				<#elseif field.formType == 'date'>
					{
					xtype : "datefield",
					name : "${field.attrName}",
					format : "Y-m-d",
					anchor : '100%',
					editable : false,
					fieldLabel : "${field.fieldComment!}",
					labelAlign : "right",
					id : 'EDIT_${field.attrName}',
					tooltip : '查询结束时间'
					}
				<#elseif field.formType == 'datetime'>
					{
					xtype : "datetimefield",
					name : "${field.attrName}",
					format : "Y-m-d",
					anchor : '100%',
					editable : false,
					fieldLabel : "${field.fieldComment!}",
					labelAlign : "right",
					id : 'EDIT_${field.attrName}',
					tooltip : '查询结束时间'
					}
				<#else>
					{
					xtype: 'textfield',
					fieldLabel: '${field.fieldComment!}',
					id: 'EDIT_${field.attrName}',
					name: '${field.attrName}',
					<#if field.formRequired && !field.primaryPk>
						allowBlank: false,
					</#if>
					<#if field.primaryPk>
						hidden: true
					</#if>
					}
				</#if>
			</#list>
			]}
			<#else>
				<#list formList as field>
					<#if field_index != 0>,</#if>
					<#if field_index % formLayout == 0>
					{ xtype: "fieldcontainer", layout: { type: "hbox" },width:860,items: [
					</#if>
					<#if field.formType == 'text'>
						{
						id: 'EDIT_${field.attrName}',labelWidth: 80, margin: "0 0 0 5", width:"400px",  labelAlign: "right",name: '${field.attrName}',xtype: 'textfield', fieldLabel: '${field.fieldComment!}',
						<#if field.formRequired && !field.primaryPk>
							allowBlank: false,
						</#if>
						<#if field.primaryPk>
							hidden: true
						</#if>
						}
					<#elseif field.formType == 'textarea'>
						{
						xtype: 'textfield',
						fieldLabel: '${field.fieldComment!}',
						id: 'EDIT_${field.attrName}',
						name: '${field.attrName}',
						<#if field.formRequired && !field.primaryPk>
							allowBlank: false,
						</#if>
						<#if field.primaryPk>
							hidden: true
						</#if>
						}
					<#elseif field.formType == 'editor'>
						{
						xtype: 'textfield',labelWidth: 80, margin: "0 0 0 5", width:"400px",  labelAlign: "right",
						fieldLabel: '${field.fieldComment!}',
						id: 'EDIT_${field.attrName}',
						name: '${field.attrName}',
						<#if field.formRequired && !field.primaryPk>
							allowBlank: false,
						</#if>
						<#if field.primaryPk>
							hidden: true
						</#if>
						}
					<#elseif field.formType == 'date'>
						{
						xtype : "datefield",labelWidth: 80, margin: "0 0 0 5", width:"400px",  labelAlign: "right",
						name : "${field.attrName}",
						format : "Y-m-d",
						anchor : '100%',
						editable : false,
						fieldLabel : "${field.fieldComment!}",
						labelAlign : "right",
						id : 'EDIT_${field.attrName}',
						tooltip : '查询结束时间'
						}
					<#elseif field.formType == 'datetime'>
						{
						xtype : "datetimefield",labelWidth: 80, margin: "0 0 0 5", width:"400px",  labelAlign: "right",
						name : "${field.attrName}",
						format : "Y-m-d",
						anchor : '100%',
						editable : false,
						fieldLabel : "${field.fieldComment!}",
						labelAlign : "right",
						id : 'EDIT_${field.attrName}',
						tooltip : '查询结束时间'
						}
					<#else>
						{
						xtype: 'textfield',labelWidth: 70, margin: "0 0 0 5", width:"400px",  labelAlign: "right",
						fieldLabel: '${field.fieldComment!}',
						id: 'EDIT_${field.attrName}',
						name: '${field.attrName}',
						<#if field.formRequired && !field.primaryPk>
							allowBlank: false,
						</#if>
						<#if field.primaryPk>
							hidden: true
						</#if>
						}
					</#if>
					<#if field_index % formLayout != 0>]}</#if>
				</#list>
				]}
			</#if>
	]});


	var windowForm = new Ext.form.Panel({
		xtype: 'form',
		reader: reader,
		bodyPadding: 0,
		bodyBorder: false,
		id: '${className}WindowForm',
		defaults : {
			xtype : "textfield",
			labelAlign : "right",
			anchor : '100%',
			margin : '4 4 4 4'
		},
		url: __ctxPath + '/${moduleName}/save${ClassName}Action.do',
		layout: 'vbox',
		items: [
			container
		]
	});
	var window = new Ext.window.Window({
		height: 400,
		width: <#if formLayout==1 >430<#else>860</#if>,
		id: 'formWindow',
		modal: true,
		layout: 'vbox',
		title: isSave ? '增加${tableComment}' : '修改${tableComment}',
		items: [windowForm],
		buttonAlign: 'center',
		buttons: [
			{
				text: "提交",
				iconCls: "btn-ok",
				handler: function () {
					var form = Ext.getCmp('${className}WindowForm').getForm();
					if (form.isValid()) {
						form.submit({
							method: "post",
							waitMsg: "正在提交数据...",
							success: function (e, f) {
								console.log(f)
								var result = f.result.result[0];
								Ext.ux.Toast.msg("操作信息", result);
								form.reset();
								var wind = Ext.getCmp("setPeron");
								if (wind != null) {
									wind.close();
								}
								window.close();
								${ClassName}View.prototype.refresh();
							},
							failure: function (e, f) {
								Ext.MessageBox.show({
									title: "操作信息",
									msg: "信息保存出错，请联系管理员！",
									buttons: Ext.MessageBox.OK,
									icon: "ext-mb-error"
								});
							}
						});
					}
				}
			}, {
				text: "关闭",
				iconCls: "btn-cancel",
				handler: function () {
					window.close();
				}
			}
		]
	});
	return window.show();
}