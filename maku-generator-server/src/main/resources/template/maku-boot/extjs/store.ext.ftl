Ext.ns('${ClassName}Store');
/** ****************${tableComment}模型****************** */
Ext.define('${className}Model', {
    extend : 'Ext.data.Model',
    fields : [<#list gridList as field><#if field.sort != 0>,</#if>'${field.fieldName}'</#list>]
});
Ext.create('Ext.data.Store', {
    pageSize : 300,
    proxy : {
        type : 'ajax',
        extraParams : {
            id: id
        },
        actionMethods : {
            read : 'POST'
        },
        url : __ctxPath + "/${moduleName}/page${ClassName}Action.do",
        reader : {
            type : 'json',
            root : 'result',
            totalProperty : 'totalCounts'
        }
    },
    storeId : '${className}Store',
    autoLoad : true,
    method : 'post',
    model : '${className}Model'
});
var store = Ext.getStore('${className}Store');
return;
})

