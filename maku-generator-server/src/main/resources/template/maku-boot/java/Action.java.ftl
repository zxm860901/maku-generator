package ${package}.${moduleName}.action;

import ${package}.${moduleName}.service.${ClassName}Service;
import ${package}.${moduleName}.model.${ClassName};
import com.fahon.mes.core.web.paging.PagingBean;
import com.fahon.mes.app.model.base.PubParty;
import com.fahon.mes.core.util.MesUtil;
import com.fahon.mes.core.web.action.BaseAction;
import cn.hutool.json.JSONUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import javax.annotation.Resource;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import java.util.UUID;
<#list importList as i>
import ${i!};
</#list>
/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
public class ${ClassName}Action extends BaseAction {

    @Resource
    private  ${ClassName}Service ${className}Service;

    /**
    * 列表
    */
    public String page(){
        PagingBean pb = getInitPagingBean(getRequest());
        String jsonStr = ${className}Service.page(pb);
        setJsonString(jsonStr);
        return SUCCESS;
    }

    /**
    * 详情
    */
    public String get(){
        String id = getRequest().getParameter("id");
        String entity = ${className}Service.getById(id);
        setJsonString(entity);
        return SUCCESS;
    }

    /**
    * 保存
    */
    public String save(){
        ${ClassName} ${className} = getVoFromRequest();
        ${className}Service.save(${className});
        setJsonString("{success:true,result:['保存成功']}");
        return SUCCESS;
    }

    /**
    * 更新
    */
    public String update(){
        ${ClassName} ${className} = getVoFromRequest();
        ${className}Service.update(${className});
        setJsonString("{success:true,result:['更新成功']}");
        return SUCCESS;
    }

    /**
    * 删除
    */
    public String delete(){
        List<String> idList = JSONUtil.toList(getRequest().getParameter("list"), String.class);
        ${className}Service.delete(idList);
        setJsonString("{success:true,result:['删除成功']}");
        return SUCCESS;
    }

    /**
     * 获取请求参数
     */
    private ${ClassName} getVoFromRequest() {
        HttpServletRequest request = getRequest();
        PubParty pubSysuser = MesUtil.getPubSysuser();
        ${ClassName} ${className} = new ${ClassName}();
    <#list fieldList as field>
        <#if field.attrName != 'createUser'&& field.attrName != 'updateUser'&& field.attrName != 'isvalidate' && field.attrName != 'createtime' && field.attrName != 'updatetime'>
        // ${field.fieldComment!}
        <#if field.formType == 'date'>
        ${className}.set${field.attrName?cap_first}(DateUtil.parse(request.getParameter("${field.attrName}"),"yyyy-MM-dd"));
        <#elseif field.formType == 'datetime'>
        <#if field.attrName != 'createtime' && field.attrName != 'updatetime'>
        ${className}.set${field.attrName?cap_first}(DateUtil.parse(request.getParameter("${field.attrName}"),"yyyy-MM-dd HH:mm:ss"));
        </#if>
        <#else >
            <#if field.primaryPk>
        //todo 更新人、更新时间、创建人、创建时间需要根据实际情况处理
        String ${field.attrName}Str = request.getParameter("${field.attrName}");
        if(StrUtil.isBlank(${field.attrName}Str)) {
            ${field.attrName}Str = UUID.randomUUID().toString();
            // 创建时间
            ${className}.setCreatetime(new Date());
            // 创建人
            ${className}.setCreateUser(pubSysuser.getPartyname());
        } else {
            // 更新时间
            ${className}.setUpdatetime(new Date());
            //更新人
            ${className}.setUpdateUser(pubSysuser.getPartyname());
        }
        ${className}.set${field.attrName?cap_first}(${field.attrName}Str);
            <#else>
        ${className}.set${field.attrName?cap_first}(${field.attrType}.valueOf(request.getParameter("${field.attrName}")));
            </#if>
        </#if>
        </#if>
    </#list>
        // 有效标识
        ${className}.setIsvalidate("1");
        return ${className};
    }
}