package ${package}.${moduleName}.convert;

import ${package}.${moduleName}.model.${ClassName};
import ${package}.${moduleName}.vo.${ClassName}VO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Mapper
public interface ${ClassName}Convert {
    ${ClassName}Convert INSTANCE = Mappers.getMapper(${ClassName}Convert.class);

    ${ClassName} convert(${ClassName}VO vo);

    ${ClassName}VO convert(${ClassName} entity);

    List<${ClassName}VO> convertList(List<${ClassName}> list);

}