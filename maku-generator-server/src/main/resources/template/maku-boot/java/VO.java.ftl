package ${package}.${moduleName}.vo;

import lombok.Data;
import java.io.Serializable;
<#list importList as i>
import ${i!};
</#list>

/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
@Data
public class ${ClassName}VO implements Serializable {
	private static final long serialVersionUID = 1L;
<#list fieldList as field>
	<#if field.fieldComment!?length gt 0>
	/**
	 * ${field.fieldComment}
	 */
	</#if>
	private ${field.attrType} ${field.attrName};
</#list>

}