<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE struts PUBLIC "-//Apache Software Foundation//DTD Struts Configuration 2.0//EN" "http://struts.apache.org/dtds/struts-2.0.dtd">
<!-- ${tableComment} -->
<struts>
	<package name="${className}Action" extends="struts-default" namespace="/${moduleName}">
		<action name="*${ClassName}Action" class="${package}.${moduleName}.action.${ClassName}Action" method="{1}">
			<result>${r'${successResultValue}'}</result>
		</action>
	</package>
</struts>
