package ${package}.${moduleName}.query;

import lombok.Data;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.date.DateUtil;

<#list importList as i>
    import ${i!};
</#list>

/**
 * ${tableComment}查询
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
@Data
public class ${ClassName}Query implements Serializable {
<#list queryList as field>
    <#if field.queryFormType == 'date' || field.queryFormType == 'datetime'>
    <#if field.fieldComment!?length gt 0>
    /**
     *${field.fieldComment} 开始
     */
    </#if>
    private ${field.attrType} ${field.attrName}_start;
    <#if field.fieldComment!?length gt 0>
    /**
     *${field.fieldComment} 结束
     */
    </#if>
    private ${field.attrType} ${field.attrName}_end;
    <#else>
    <#if field.fieldComment!?length gt 0>
    /**
     *${field.fieldComment}
     */
    </#if>
    private ${field.attrType} ${field.attrName};
    </#if>
</#list>

}