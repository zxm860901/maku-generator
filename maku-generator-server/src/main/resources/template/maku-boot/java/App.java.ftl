<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE beans PUBLIC "-//SPRING//DTD BEAN 2.0//EN" "http://www.springframework.org/dtd/spring-beans-2.0.dtd">
<beans>
	<!-- ${tableComment} -->
	<bean id="${className}Dao" class=" ${package}.${moduleName}.dao.impl.${ClassName}DaoImpl" parent="baseDao" />
	<bean id="${className}Service" class="${package}.${moduleName}.service.impl.${ClassName}ServiceImpl">
		<constructor-arg index="0" ref="${className}Dao" />
	</bean>
</beans>