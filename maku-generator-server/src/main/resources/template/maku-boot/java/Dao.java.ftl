package ${package}.${moduleName}.dao;

import com.fahon.mes.core.dao.BaseDao;
import com.fahon.mes.core.web.paging.PagingBean;
import ${package}.${moduleName}.model.${ClassName};

import java.util.List;

/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
public interface ${ClassName}Dao extends BaseDao<${ClassName}> {
	String page(PagingBean pb);

	void delete(List<String> idList);

	String getById(String id);

	List<${ClassName}> getByIds(List<String> idList);

	void insertBatch(List<${ClassName}> dataList);

	void updateBatch(List<${ClassName}> dataList);
}