package ${package}.${moduleName}.service;

import ${package}.${moduleName}.model.${ClassName};

import com.fahon.mes.core.service.BaseService;
import com.fahon.mes.core.web.paging.PagingBean;

import java.util.List;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
public interface ${ClassName}Service extends BaseService<${ClassName}> {

    String page(PagingBean pb);

    void update(${ClassName} ${className});

    void delete(List<String> idList);

    String getById(String id);

    List<${ClassName}> getByIds(List<String> idList)

    void insertBatch(List<${ClassName}> dataList);

    void updateBatch(List<${ClassName}> dataList);
}