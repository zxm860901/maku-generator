package ${package}.${moduleName}.model;

import java.io.Serializable;
import lombok.Data;
<#list importList as i>
import ${i!};
</#list>
<#if baseClass??>
import ${baseClass.packageName}.${baseClass.code};
</#if>

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
<#if baseClass??>@EqualsAndHashCode(callSuper=false)</#if>
@Data
public class ${ClassName} implements Serializable {
<#list fieldList as field>
<#if !field.baseField>
	<#if field.fieldComment!?length gt 0>
	/**
	* ${field.fieldComment}
	*/
	</#if>
	private ${field.attrType} ${field.attrName};
</#if>

</#list>
}