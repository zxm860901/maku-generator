package ${package}.${moduleName}.service.impl;

import ${package}.${moduleName}.convert.${ClassName}Convert;
import ${package}.${moduleName}.model.${ClassName};
import ${package}.${moduleName}.vo.${ClassName}VO;
import ${package}.${moduleName}.dao.${ClassName}Dao;
import ${package}.${moduleName}.service.${ClassName}Service;

import com.fahon.mes.core.service.impl.BaseServiceImpl;
import com.fahon.mes.core.web.paging.PagingBean;

import javax.annotation.Resource;
import java.util.List;

/**
 * ${tableComment}
 *
 * @author ${author} ${email}
 * @since ${version} ${date}
 */
public class ${ClassName}ServiceImpl extends BaseServiceImpl<${ClassName}> implements ${ClassName}Service {

    @Resource
    private ${ClassName}Dao ${className}Dao;

    public ${ClassName}ServiceImpl(${ClassName}Dao dao) {
        super(dao);
    }


    @Override
    public String page(PagingBean pb) {
        return ${className}Dao.page(pb);
    }

    @Override
    public void update(${ClassName} ${className}) {
        ${className}Dao.merge(${className});
    }

    @Override
    public void delete(List<String> idList) {
        ${className}Dao.delete(idList);
    }

    @Override
    public String getById(String id) {
        return ${className}Dao.getById(id);
    }

    @Override
    public List<${ClassName}> getByIds(List<String> ids) {
        return ${className}Dao.getByIds(ids);
    }

    @Override
    public void insertBatch(List<${ClassName}> dataList) {
        ${className}Dao.insertBatch(dataList);
    }

    @Override
    public void updateBatch(List<${ClassName}> dataList) {
        ${className}Dao.updateBatch(dataList);
    }
}