package ${package}.${moduleName}.dao.impl;

import com.fahon.mes.core.dao.impl.BaseDaoImpl;
import com.fahon.mes.core.web.paging.PagingBean;

import ${package}.${moduleName}.dao.${ClassName}Dao;
import ${package}.${moduleName}.model.${ClassName};
import ${package}.${moduleName}.vo.${ClassName}VO;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.ObjectUtil;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
* ${tableComment}
*
* @author ${author} ${email}
* @since ${version} ${date}
*/
public class ${ClassName}DaoImpl extends BaseDaoImpl<${ClassName}> implements ${ClassName}Dao {
    private ${ClassName}DaoImpl() {
        super(${ClassName}.class);
    }

    @Override
    public String page(PagingBean pb) {
        String sql = "select "
        <#list fieldList as field>
            + "<#if field_index !=0>,</#if><#if field.formType == 'date'>to_char(${field.fieldName}, 'yyyy-mm-dd') as ${field.fieldName} <#elseif field.formType == 'datetime'>to_char(${field.fieldName}, 'yyyy-mm-dd HH24:mi:ss') as ${field.fieldName}<#else >${field.fieldName}</#if>"
        </#list>
            + " from ${tableName} "
            +getWrapper();
        return findByJDBC(pb,sql);
    }

    @Override
    public void delete(List<String> idList) {
        String sql = "update  ${tableName} set ISVALIDATE = '0' where ${primaryList[0].fieldName} in ("+ idList.stream().map(e->"'"+e+"'").collect(Collectors.joining(",")) +")";
        jdbcTemplate.execute(sql);
    }
    @Override
    public List<${ClassName}> getByIds(List<String> idList) {
        String sql = "from ${tableName} set ISVALIDATE = '0' where ${primaryList[0].fieldName} in ("+ idList.stream().map(e->"'"+e+"'").collect(Collectors.joining(",")) +")";
        return findByHql(hql);
    }

    @Override
    public String getById(String id) {
        String sql = "select "
        <#list fieldList as field>
            + "<#if field_index !=0>,</#if><#if field.formType == 'date'>to_char(${field.fieldName}, 'yyyy-mm-dd') as ${field.fieldName} <#elseif field.formType == 'datetime'>to_char(${field.fieldName}, 'yyyy-mm-dd HH24:mi:ss') as ${field.fieldName}<#else >${field.fieldName}</#if>"
        </#list>
        + " from ${tableName} where ${primaryList[0].fieldName} = '" + id + "'";
        return findByJDBC(sql);
    }

    @Override
    public void insertBatch(List<${ClassName}> dataList) {
        String sql = "INSERT INTO ${tableName} (<#list fieldList as field><#if field_index !=0>,</#if>${field.fieldName}</#list>)"
            + "VALUES (<#list fieldList as field><#if field_index !=0>,</#if>:${field.attrName}</#list>)";
        Session session = getSession();
        Transaction transaction = session.beginTransaction();
        try {
            // 创建Query对象，并设置SQL语句
            Query query = session.createSQLQuery(sql);
            for (${ClassName} ${className} : dataList) {
                // 设置参数值
                <#list fieldList as field>
                query.setParameter("${field.attrName}",ObjectUtil.defaultIfNull(${className}.get${field.upperAttrName}(),<#if field.attrType=='String'>""<#elseif field.attrType=='Date'>new Date()<#else >0</#if>));
                </#list>
                // 执行更新操作
                query.executeUpdate();
            }
            // 提交事务
            transaction.commit();
        } catch (Exception e) {
            // 回滚事务
            transaction.rollback();
            e.printStackTrace();
        } finally {
            // 关闭Session
            session.close();
        }
    }

        @Override
        public void updateBatch(List<${ClassName}> dataList) {
            Session session = getSession();
            Transaction transaction = session.beginTransaction();
            try {
                for (${ClassName} ${className} : dataList) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("UPDATE ${tableName} SET UPDATETIME = sysdate");
                    <#list fieldList as field>
                    if (ObjectUtil.isNotEmpty(${className}.get${field.upperAttrName}())) {
                        sb.append(", ${field.fieldName} = :${field.attrName}");
                    }
                    </#list>
                    sb.append(" where <#list fieldList as field><#if field.primaryPk> ${field.fieldName} = '").append(${className}.get${field.upperAttrName}()).append("'")</#if></#list>;
                    // 创建Query对象，并设置SQL语句
                    Query query = session.createSQLQuery(sb.toString());
                    <#list fieldList as field>
                    if (ObjectUtil.isNotEmpty(${className}.get${field.upperAttrName}())) {
                        query.setParameter("${field.attrName}",${className}.get${field.upperAttrName}());
                    }
                    </#list>
                    query.executeUpdate();
                }
                // 提交事务
                transaction.commit();
            } catch (Exception e) {
                // 回滚事务
                transaction.rollback();
                e.printStackTrace();
            } finally {
                // 关闭Session
                session.close();
            }
        }


    private String getWrapper(){
        HttpServletRequest request = ServletActionContext.getRequest();
        StringBuffer wrapper = new StringBuffer();
    <#list queryList as field>
        <#if field.queryFormType == 'date' || field.queryFormType == 'datetime'>
        String ${field.attrName}_start = request.getParameter("${field.attrName}_start");
        String ${field.attrName}_end = request.getParameter("${field.attrName}_end");
        // 日期有值,进行条件筛选
        boolean ${field.attrName}Flag = true;
        if(!StrUtil.isAllBlank(${field.attrName}_start,${field.attrName}_end)) {
            if(StrUtil.isBlank(${field.attrName}_start)) {
                ${field.attrName}_start = StrUtil.replace(${field.attrName}_end,0,10,"0001-01-01");
            }
            if(StrUtil.isBlank(${field.attrName}_end)) {
                ${field.attrName}_end = StrUtil.replace(${field.attrName}_start,0,10,"9999-01-01");
            }
        } else {
            ${field.attrName}Flag = false;
        }
        <#else>
        String ${field.attrName} = request.getParameter("${field.attrName}");
        </#if>
    </#list>
    <#list queryList as field>
        <#if field_index==0>wrapper.append("where ISVALIDATE = '1'");</#if>
        <#if field.queryFormType == 'date'>
            if (${field.attrName}Flag) {
                wrapper.append("and ${field.attrName} between to_date('"+StrUtil.subWithLength(${field.attrName}_start,0,10)+"','yyyy-mm-dd') and to_date('" + StrUtil.subWithLength(${field.attrName}_end,0,10)+"','yyyy-mm-dd')\n");
            }
        <#elseif field.queryFormType == 'datetime'>
            if (${field.attrName}Flag) {
                wrapper.append("and ${field.attrName} between to_date('"+StrUtil.replace(${field.attrName}_start,"T"," ")+"','yyyy-mm-dd HH24:mi:ss') and to_date('"+StrUtil.replace(${field.attrName}_end,"T"," ")+"','yyyy-mm-dd HH24:mi:ss')\n");
            }
        <#elseif field.queryType == '='
        || field.queryFormType == '!='
        || field.queryFormType == '>'
        || field.queryFormType == '<'
        || field.queryFormType == '>='
        || field.queryFormType == '<='>
            if (ObjectUtil.isNotEmpty(${field.attrName})) {
                wrapper.append("and ${field.attrName} ${field.queryType} '"+${field.attrName}+"'\n");
            }
        <#elseif field.queryType == 'like'>
            if (ObjectUtil.isNotEmpty(${field.attrName})) {
                wrapper.append("and ${field.attrName} like '%"+${field.attrName}+"%'\n");
            }
        <#elseif field.queryType == 'left like'>
            if (ObjectUtil.isNotEmpty(${field.attrName})) {
                wrapper.append("and ${field.attrName} like '"+"${field.attrName}+"%'\n");
            }
        <#elseif field.queryType == 'right like'>
            if (ObjectUtil.isNotEmpty(${field.attrName})) {
                wrapper.append("and ${field.attrName} like '%"+${field.attrName}+"'\n");
            }
        </#if>
    </#list>
        return wrapper.toString();
    }
}